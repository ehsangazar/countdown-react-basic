import React, { useEffect, useState } from 'react';
import './App.css';

function App() {
  const [start,setStart] = useState(false)
  const [count,setCount] = useState(0)

  const [hour1,setHour1] = useState(2);
  const [hour2, setHour2] = useState(4);

  const [minute1, setMinute1] = useState(0);
  const [minute2, setMinute2] = useState(0);

  const [second1, setSecond1] = useState(0);
  const [second2, setSecond2] = useState(2);

  useEffect(()=>{
    setStart(true)
  },[])

  useEffect(()=>{
    setTimeout(() => {
      setCount(count+1)

      if (second2 > 0 ) {
        setSecond2(second2-1)
        return
      }

      if (second1 > 0) {
        setSecond1(second1-1)
        setSecond2(9)
        return
      }

      if (minute2 > 0) {
        setMinute2(minute2 - 1)
        setSecond1(5)
        setSecond2(9)
        return
      }

      if (minute1 > 0) {
        setMinute1(minute1 - 1)
        setMinute2(9)
        setSecond1(5)
        setSecond2(9)
        return
      }

      if (hour2 > 0) {
        setHour2(hour2 - 1)
        setMinute1(5)
        setMinute2(9)
        setSecond1(5)
        setSecond2(9)
        return
      }

      if (hour1 > 0) {
        setHour1(hour1 - 1)
        setHour2(9)
        setMinute1(5)
        setMinute2(9)
        setSecond1(5)
        setSecond2(9)
        return
      }


    }, 1000);
  },[start,count])

  return (
    <div className="App">
      <div className="wrap">
        <h1>Countdown By <strong>Ehsan Gazar</strong></h1>

        <div className="countdown">
          <div className="bloc-time hours" data-init-value="24">
            <span className="count-title">Hours</span>

            <div className="figure hours hours-1">
              <span className="top">{hour1}</span>
              <span className="top-back">
                <span>{hour1}</span>
              </span>
              <span className="bottom">{hour1}</span>
              <span className="bottom-back">
                <span>{hour1}</span>
              </span>
            </div>

            <div className="figure hours hours-2">
              <span className="top">{hour2}</span>
              <span className="top-back">
                <span>{hour2}</span>
              </span>
              <span className="bottom">{hour2}</span>
              <span className="bottom-back">
                <span>{hour2}</span>
              </span>
            </div>
          </div>

          <div className="bloc-time min" data-init-value="0">
            <span className="count-title">Minutes</span>

            <div className="figure min min-1">
              <span className="top">{minute1}</span>
              <span className="top-back">
                <span>{minute1}</span>
              </span>
              <span className="bottom">{minute1}</span>
              <span className="bottom-back">
                <span>{minute1}</span>
              </span>
            </div>

            <div className="figure min min-2">
              <span className="top">{minute2}</span>
              <span className="top-back">
                <span>{minute2}</span>
              </span>
              <span className="bottom">{minute2}</span>
              <span className="bottom-back">
                <span>{minute2}</span>
              </span>
            </div>
          </div>

          <div className="bloc-time sec" data-init-value="0">
            <span className="count-title">Seconds</span>

            <div className="figure sec sec-1">
              <span className="top">{second1}</span>
              <span className="top-back">
                <span>{second1}</span>
              </span>
              <span className="bottom">{second1}</span>
              <span className="bottom-back">
                <span>{second1}</span>
              </span>
            </div>

            <div className="figure sec sec-2">
              <span className="top">{second2}</span>
              <span className="top-back">
                <span>{second2}</span>
              </span>
              <span className="bottom">{second2}</span>
              <span className="bottom-back">
                <span>{second2}</span>
              </span>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
